export type StateType = 'APROBADO' | 'DESAPROBADO' | 'SUSPENDIDO';
export type ActionType = 'form' | 'detail' | 'list';

export class Student {
  code: number;
  name: string;
  age: number;
  state: StateType;
}
