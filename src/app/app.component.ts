import { Component } from '@angular/core';
import { StudentService } from 'src/app/student/student.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'ProyectoClase2';
  constructor(public studentService: StudentService) {
  }
}
