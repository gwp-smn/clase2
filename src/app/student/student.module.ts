import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { StudentListComponent } from './student-list/student-list.component';
import { StudentDetailComponent } from './student-detail/student-detail.component';
import { StudentFormComponent } from './student-form/student-form.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule
  ],
  declarations: [StudentListComponent, StudentDetailComponent, StudentFormComponent],
  exports: [StudentListComponent, StudentDetailComponent, StudentFormComponent]
})
export class StudentModule { }
