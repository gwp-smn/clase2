import { Injectable } from '@angular/core';
import { Student, ActionType } from 'src/app/models/student';
import { Action } from 'rxjs/internal/scheduler/Action';

@Injectable({
  providedIn: 'root'
})
export class StudentService {

  action: ActionType = 'list';

  private students: Student[] = [];
  currentStudent: Student;

  constructor() {
    this.students = STUDENTS.slice();
  }

  search(value: string): Student[] {
    return this.students.filter(student => student.name.toLowerCase().includes(value.toLowerCase()));
  }

  getAll(): Student[] {
    return this.students;
  }

  get(code: number): Student {
    return this.students.find(o => o.code === code);
  }

  save(std: Student) {
    this.students.push(std);
  }

  edit(std: Student) {
    this.students[this.students.findIndex(o => o.code === std.code)] = std;
  }

  delete(code: number) {
    this.students.splice(this.students.findIndex(o => o.code === code), 1);
  }
}

const STUDENTS: Student[] = [
  { code: 100, name: 'Lorena Tapia', age: 27, state: 'APROBADO' },
  { code: 111, name: 'Juan Perez', age: 32, state: 'APROBADO' },
  { code: 222, name: 'Noemi Sanchez', age: 25, state: 'SUSPENDIDO' },
  { code: 333, name: 'Jose Gonzales', age: 29, state: 'DESAPROBADO' },
  { code: 444, name: 'Miguel Cruz', age: 24, state: 'SUSPENDIDO' },
  { code: 555, name: 'Guillermo Tapia', age: 35, state: 'APROBADO' },
  { code: 666, name: 'Marianela Perez', age: 33, state: 'DESAPROBADO' },
  { code: 777, name: 'Abigail Wayar', age: 23, state: 'APROBADO' }
];
