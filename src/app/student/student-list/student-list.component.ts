import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { Student, StateType } from 'src/app/models/student';
import { StudentService } from 'src/app/student/student.service';

@Component({
  selector: 'app-student-list',
  templateUrl: './student-list.component.html',
  styleUrls: ['./student-list.component.css']
})
export class StudentListComponent implements OnInit {

  title = 'Lista de alumnos';
  placeholder = 'Ingrese nombre del alumno';
  nameToSearch = '';
  students: Student[] = [];

  constructor(
    private studentService: StudentService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    this.loadStudents();
  }

  showDetail(std: Student) {
    this.router.navigateByUrl(`/student/detail/${std.code}`);
  }

  showFormEdit(std: Student) {
    this.studentService.currentStudent = std;
    this.studentService.action = 'form';
  }

  showFormNew() {
    this.studentService.currentStudent = null;
    this.studentService.action = 'form';
  }

  private loadStudents() {
    this.students = this.studentService.getAll();
  }

  searchStudents() {
    this.students = this.studentService.search(this.nameToSearch);
  }

  clean() {
    this.nameToSearch = '';
    this.students = this.studentService.getAll();
  }

  getClass(state: StateType): string {
    switch (state) {
      case 'APROBADO':
        return 'table-success';
      case 'DESAPROBADO':
        return 'table-danger';
      case 'SUSPENDIDO':
        return 'table-secondary';
      default: return '';
    }
  }

}
