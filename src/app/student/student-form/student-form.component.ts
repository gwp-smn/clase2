import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { StudentService } from '../student.service';
import { Student } from 'src/app/models/student';

@Component({
  selector: 'app-student-form',
  templateUrl: './student-form.component.html',
  styleUrls: ['./student-form.component.css']
})
export class StudentFormComponent implements OnInit {

  student: Student;
  isNew: boolean;

  constructor(private studentService: StudentService) {
    this.isNew = true;
  }

  ngOnInit() {
    if (this.studentService.currentStudent) {
      this.student = this.studentService.currentStudent;
      this.isNew = false;
    }
  }

  showList() {
    this.studentService.action = 'list';
  }

  save() {
    if (this.isNew) {
      this.studentService.save(this.student);
    } else {
      this.studentService.edit(this.student)
    }
    this.showList();
  }
}
