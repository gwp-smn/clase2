import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { StudentService } from '../student.service';
import { Student } from 'src/app/models/student';

@Component({
  selector: 'app-student-detail',
  templateUrl: './student-detail.component.html',
  styleUrls: ['./student-detail.component.css']
})
export class StudentDetailComponent implements OnInit {

  student: Student;

  constructor(
    private studentService: StudentService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    const code = +this.activatedRoute.snapshot.paramMap.get('code');
    this.student = this.studentService.get(code);
  }

  showList() {
    this.router.navigateByUrl('/student');
  }

}
